# Swagger generator for Laravel

This plugin is designed to gather information and generate documentation about 
your Rest-Api while passing the tests. The principle of operation is based on 
the fact that the special Middleware installed on the Route for which you want 
to collect information that after the completion of all tests 
generated Swagger-file. In addition this plug-in is able to draw Swagger-template 
to display the generated documentation for a config.

## Instalation

### Composer
 1. `composer require cdonut/swagger-generator`
 2. run `php artisan vendor:publish`
 3. Add middleware **\Cdonut\SwaggerGenerator\Http\Middleware\SwaggerGeneratorMiddleware::class** to *Http/Kernel.php*.
 4. Set **\Cdonut\SwaggerGenerator\Tests\SwaggerGeneratorTestCase** as parent of your TestCase in *tests/TestCase.php*
 5. In *config/auto-doc.php* you can specify enabling of plugin, info of your project, 
 some defaults descriptions and route for rendering of documentation. 
 6. In *phpunit.xml* file you should add extension
    `
     <extensions>
        <extension class="Cdonut\SwaggerGenerator\Unit\Extension" />
    </extensions>
    `
 
