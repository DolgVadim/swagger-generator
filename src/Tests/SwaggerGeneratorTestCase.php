<?php

namespace Cdonut\SwaggerGenerator\Tests;

use Cdonut\SwaggerGenerator\DataWriter;
use Illuminate\Foundation\Testing\TestCase;
use Cdonut\SwaggerGenerator\Services\SwaggerService;
use Cdonut\SwaggerGenerator\Http\Middleware\SwaggerGeneratorMiddleware;
use Illuminate\Support\Facades\Cache;
use PHPUnit\Framework\Test;

class SwaggerGeneratorTestCase extends TestCase
{
    protected $docService;

    public function setUp(): void
    {
        parent::setUp();

        $this->docService = app(SwaggerService::class);
    }

    public function createApplication()
    {
        parent::createApplication();
    }

    /**
     * Disabling documentation collecting on current test
     */
    public function skipDocumentationCollecting()
    {
        SwaggerGeneratorMiddleware::$skipped = true;
    }
}
