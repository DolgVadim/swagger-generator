<?php

namespace Cdonut\SwaggerGenerator\Traits;

/**
 * @deprecated
*/
trait SwaggerGeneratorRequestTrait {

    static public function getRules() {
        return (new static)->rules();
    }

}
