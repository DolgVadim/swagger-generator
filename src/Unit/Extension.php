<?php
namespace Cdonut\SwaggerGenerator\Unit;

use PHPUnit\Runner\AfterLastTestHook;

final class Extension implements AfterLastTestHook
{
    public function executeAfterLastTest(): void
    {
         \Cdonut\SwaggerGenerator\Services\SwaggerService::saveData();
    }
}
