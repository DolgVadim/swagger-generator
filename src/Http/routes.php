<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.08.16
 * Time: 11:37
 */

use Cdonut\SwaggerGenerator\Http\Controllers\SwaggerGeneratorController;

Route::get(config('swagger-generator.route'), ['uses' => SwaggerGeneratorController::class.'@index']);
