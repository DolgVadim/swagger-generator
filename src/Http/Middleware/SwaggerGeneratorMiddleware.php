<?php

namespace Cdonut\SwaggerGenerator\Http\Middleware;

use Closure;
use Cdonut\SwaggerGenerator\Services\SwaggerService;

/**
 * @property SwaggerService $service
*/
class SwaggerGeneratorMiddleware
{
    protected $service;
    public static $skipped = false;

    public function __construct()
    {
        $this->service = app(SwaggerService::class);
    }

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if ((config('app.env') == 'testing') && !self::$skipped) {
            $this->service->addData($request, $response);

        }

        self::$skipped = false;

        return $response;
    }
}
