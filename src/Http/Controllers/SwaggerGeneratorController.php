<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 29.08.16
 * Time: 11:29
 */

namespace Cdonut\SwaggerGenerator\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Cdonut\SwaggerGenerator\Services\SwaggerService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SwaggerGeneratorController extends BaseController
{
    public function index()
    {
        return view('swagger-generator::documentation');
    }
}
