<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 26.08.16
 * Time: 11:49
 */

namespace Cdonut\SwaggerGenerator;

use Illuminate\Support\ServiceProvider;

class SwaggerGeneratorServiceProvider extends ServiceProvider
{
    public function boot() {
        $this->publishes([
            __DIR__.'/config/swagger-generator.php' => config_path('swagger-generator.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/Views/swagger-description.blade.php' => resource_path('views/swagger-description.blade.php'),
        ], 'view');

        $this->publishes([
            __DIR__.'/Views/swagger' => public_path('swagger-generator'),
        ], 'public');

        if (! $this->app->routesAreCached()) {
            require __DIR__.'/Http/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/Views', 'swagger-generator');
    }

    public function register()
    {

    }
}
